package com.esports.dao;

import com.esports.bean.domain.MatchCourseGroup;
import com.esports.bean.domain.MatchCourseGroupExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchCourseGroupMapper {
    int countByExample(MatchCourseGroupExample example);

    int deleteByExample(MatchCourseGroupExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchCourseGroup record);

    int insertSelective(MatchCourseGroup record);

    List<MatchCourseGroup> selectByExample(MatchCourseGroupExample example);

    MatchCourseGroup selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchCourseGroup record, @Param("example") MatchCourseGroupExample example);

    int updateByExample(@Param("record") MatchCourseGroup record, @Param("example") MatchCourseGroupExample example);

    int updateByPrimaryKeySelective(MatchCourseGroup record);

    int updateByPrimaryKey(MatchCourseGroup record);
}