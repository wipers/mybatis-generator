package com.esports.dao;

import com.esports.bean.domain.TopicComment;
import com.esports.bean.domain.TopicCommentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TopicCommentMapper {
    int countByExample(TopicCommentExample example);

    int deleteByExample(TopicCommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TopicComment record);

    int insertSelective(TopicComment record);

    List<TopicComment> selectByExample(TopicCommentExample example);

    TopicComment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TopicComment record, @Param("example") TopicCommentExample example);

    int updateByExample(@Param("record") TopicComment record, @Param("example") TopicCommentExample example);

    int updateByPrimaryKeySelective(TopicComment record);

    int updateByPrimaryKey(TopicComment record);
}