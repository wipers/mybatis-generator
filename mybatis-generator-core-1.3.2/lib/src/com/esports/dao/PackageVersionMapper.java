package com.esports.dao;

import com.esports.bean.domain.PackageVersion;
import com.esports.bean.domain.PackageVersionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PackageVersionMapper {
    int countByExample(PackageVersionExample example);

    int deleteByExample(PackageVersionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PackageVersion record);

    int insertSelective(PackageVersion record);

    List<PackageVersion> selectByExample(PackageVersionExample example);

    PackageVersion selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PackageVersion record, @Param("example") PackageVersionExample example);

    int updateByExample(@Param("record") PackageVersion record, @Param("example") PackageVersionExample example);

    int updateByPrimaryKeySelective(PackageVersion record);

    int updateByPrimaryKey(PackageVersion record);
}