package com.esports.dao;

import com.esports.bean.domain.MatchComment;
import com.esports.bean.domain.MatchCommentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchCommentMapper {
    int countByExample(MatchCommentExample example);

    int deleteByExample(MatchCommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchComment record);

    int insertSelective(MatchComment record);

    List<MatchComment> selectByExample(MatchCommentExample example);

    MatchComment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchComment record, @Param("example") MatchCommentExample example);

    int updateByExample(@Param("record") MatchComment record, @Param("example") MatchCommentExample example);

    int updateByPrimaryKeySelective(MatchComment record);

    int updateByPrimaryKey(MatchComment record);
}