package com.esports.dao;

import com.esports.bean.domain.UserPushLog;
import com.esports.bean.domain.UserPushLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserPushLogMapper {
    int countByExample(UserPushLogExample example);

    int deleteByExample(UserPushLogExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserPushLog record);

    int insertSelective(UserPushLog record);

    List<UserPushLog> selectByExample(UserPushLogExample example);

    UserPushLog selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserPushLog record, @Param("example") UserPushLogExample example);

    int updateByExample(@Param("record") UserPushLog record, @Param("example") UserPushLogExample example);

    int updateByPrimaryKeySelective(UserPushLog record);

    int updateByPrimaryKey(UserPushLog record);
}