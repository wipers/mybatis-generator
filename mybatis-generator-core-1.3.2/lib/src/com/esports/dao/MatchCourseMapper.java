package com.esports.dao;

import com.esports.bean.domain.MatchCourse;
import com.esports.bean.domain.MatchCourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchCourseMapper {
    int countByExample(MatchCourseExample example);

    int deleteByExample(MatchCourseExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchCourse record);

    int insertSelective(MatchCourse record);

    List<MatchCourse> selectByExample(MatchCourseExample example);

    MatchCourse selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchCourse record, @Param("example") MatchCourseExample example);

    int updateByExample(@Param("record") MatchCourse record, @Param("example") MatchCourseExample example);

    int updateByPrimaryKeySelective(MatchCourse record);

    int updateByPrimaryKey(MatchCourse record);
}