package com.esports.dao;

import com.esports.bean.domain.VideoCommentLikeUser;
import com.esports.bean.domain.VideoCommentLikeUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VideoCommentLikeUserMapper {
    int countByExample(VideoCommentLikeUserExample example);

    int deleteByExample(VideoCommentLikeUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VideoCommentLikeUser record);

    int insertSelective(VideoCommentLikeUser record);

    List<VideoCommentLikeUser> selectByExample(VideoCommentLikeUserExample example);

    VideoCommentLikeUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VideoCommentLikeUser record, @Param("example") VideoCommentLikeUserExample example);

    int updateByExample(@Param("record") VideoCommentLikeUser record, @Param("example") VideoCommentLikeUserExample example);

    int updateByPrimaryKeySelective(VideoCommentLikeUser record);

    int updateByPrimaryKey(VideoCommentLikeUser record);
}