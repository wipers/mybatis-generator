package com.esports.dao;

import com.esports.bean.domain.NewGalley;
import com.esports.bean.domain.NewGalleyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NewGalleyMapper {
    int countByExample(NewGalleyExample example);

    int deleteByExample(NewGalleyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(NewGalley record);

    int insertSelective(NewGalley record);

    List<NewGalley> selectByExample(NewGalleyExample example);

    NewGalley selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") NewGalley record, @Param("example") NewGalleyExample example);

    int updateByExample(@Param("record") NewGalley record, @Param("example") NewGalleyExample example);

    int updateByPrimaryKeySelective(NewGalley record);

    int updateByPrimaryKey(NewGalley record);
}