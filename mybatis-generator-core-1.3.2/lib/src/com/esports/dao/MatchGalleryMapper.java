package com.esports.dao;

import com.esports.bean.domain.MatchGallery;
import com.esports.bean.domain.MatchGalleryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchGalleryMapper {
    int countByExample(MatchGalleryExample example);

    int deleteByExample(MatchGalleryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchGallery record);

    int insertSelective(MatchGallery record);

    List<MatchGallery> selectByExample(MatchGalleryExample example);

    MatchGallery selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchGallery record, @Param("example") MatchGalleryExample example);

    int updateByExample(@Param("record") MatchGallery record, @Param("example") MatchGalleryExample example);

    int updateByPrimaryKeySelective(MatchGallery record);

    int updateByPrimaryKey(MatchGallery record);
}