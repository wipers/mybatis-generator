package com.esports.dao;

import com.esports.bean.domain.MatchUnitLive;
import com.esports.bean.domain.MatchUnitLiveExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchUnitLiveMapper {
    int countByExample(MatchUnitLiveExample example);

    int deleteByExample(MatchUnitLiveExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchUnitLive record);

    int insertSelective(MatchUnitLive record);

    List<MatchUnitLive> selectByExample(MatchUnitLiveExample example);

    MatchUnitLive selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchUnitLive record, @Param("example") MatchUnitLiveExample example);

    int updateByExample(@Param("record") MatchUnitLive record, @Param("example") MatchUnitLiveExample example);

    int updateByPrimaryKeySelective(MatchUnitLive record);

    int updateByPrimaryKey(MatchUnitLive record);
}