package com.esports.dao;

import com.esports.bean.domain.NewsContent;
import com.esports.bean.domain.NewsContentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NewsContentMapper {
    int countByExample(NewsContentExample example);

    int deleteByExample(NewsContentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(NewsContent record);

    int insertSelective(NewsContent record);

    List<NewsContent> selectByExample(NewsContentExample example);

    NewsContent selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") NewsContent record, @Param("example") NewsContentExample example);

    int updateByExample(@Param("record") NewsContent record, @Param("example") NewsContentExample example);

    int updateByPrimaryKeySelective(NewsContent record);

    int updateByPrimaryKey(NewsContent record);
}