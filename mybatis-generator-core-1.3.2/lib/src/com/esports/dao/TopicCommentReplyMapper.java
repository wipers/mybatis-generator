package com.esports.dao;

import com.esports.bean.domain.TopicCommentReply;
import com.esports.bean.domain.TopicCommentReplyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TopicCommentReplyMapper {
    int countByExample(TopicCommentReplyExample example);

    int deleteByExample(TopicCommentReplyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TopicCommentReply record);

    int insertSelective(TopicCommentReply record);

    List<TopicCommentReply> selectByExample(TopicCommentReplyExample example);

    TopicCommentReply selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TopicCommentReply record, @Param("example") TopicCommentReplyExample example);

    int updateByExample(@Param("record") TopicCommentReply record, @Param("example") TopicCommentReplyExample example);

    int updateByPrimaryKeySelective(TopicCommentReply record);

    int updateByPrimaryKey(TopicCommentReply record);
}