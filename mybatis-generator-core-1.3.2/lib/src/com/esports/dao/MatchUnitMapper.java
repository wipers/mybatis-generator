package com.esports.dao;

import com.esports.bean.domain.MatchUnit;
import com.esports.bean.domain.MatchUnitExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchUnitMapper {
    int countByExample(MatchUnitExample example);

    int deleteByExample(MatchUnitExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchUnit record);

    int insertSelective(MatchUnit record);

    List<MatchUnit> selectByExample(MatchUnitExample example);

    MatchUnit selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchUnit record, @Param("example") MatchUnitExample example);

    int updateByExample(@Param("record") MatchUnit record, @Param("example") MatchUnitExample example);

    int updateByPrimaryKeySelective(MatchUnit record);

    int updateByPrimaryKey(MatchUnit record);
}