package com.esports.dao;

import com.esports.bean.domain.ApnsToken;
import com.esports.bean.domain.ApnsTokenExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApnsTokenMapper {
    int countByExample(ApnsTokenExample example);

    int deleteByExample(ApnsTokenExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ApnsToken record);

    int insertSelective(ApnsToken record);

    List<ApnsToken> selectByExample(ApnsTokenExample example);

    ApnsToken selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ApnsToken record, @Param("example") ApnsTokenExample example);

    int updateByExample(@Param("record") ApnsToken record, @Param("example") ApnsTokenExample example);

    int updateByPrimaryKeySelective(ApnsToken record);

    int updateByPrimaryKey(ApnsToken record);
}