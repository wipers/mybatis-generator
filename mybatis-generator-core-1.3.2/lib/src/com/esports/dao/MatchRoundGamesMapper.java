package com.esports.dao;

import com.esports.bean.domain.MatchRoundGames;
import com.esports.bean.domain.MatchRoundGamesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchRoundGamesMapper {
    int countByExample(MatchRoundGamesExample example);

    int deleteByExample(MatchRoundGamesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchRoundGames record);

    int insertSelective(MatchRoundGames record);

    List<MatchRoundGames> selectByExample(MatchRoundGamesExample example);

    MatchRoundGames selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchRoundGames record, @Param("example") MatchRoundGamesExample example);

    int updateByExample(@Param("record") MatchRoundGames record, @Param("example") MatchRoundGamesExample example);

    int updateByPrimaryKeySelective(MatchRoundGames record);

    int updateByPrimaryKey(MatchRoundGames record);
}