package com.esports.dao;

import com.esports.bean.domain.NewsCommentLikeUser;
import com.esports.bean.domain.NewsCommentLikeUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NewsCommentLikeUserMapper {
    int countByExample(NewsCommentLikeUserExample example);

    int deleteByExample(NewsCommentLikeUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(NewsCommentLikeUser record);

    int insertSelective(NewsCommentLikeUser record);

    List<NewsCommentLikeUser> selectByExample(NewsCommentLikeUserExample example);

    NewsCommentLikeUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") NewsCommentLikeUser record, @Param("example") NewsCommentLikeUserExample example);

    int updateByExample(@Param("record") NewsCommentLikeUser record, @Param("example") NewsCommentLikeUserExample example);

    int updateByPrimaryKeySelective(NewsCommentLikeUser record);

    int updateByPrimaryKey(NewsCommentLikeUser record);
}