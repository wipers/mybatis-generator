package com.esports.dao;

import com.esports.bean.domain.MatchCommentLikeUser;
import com.esports.bean.domain.MatchCommentLikeUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MatchCommentLikeUserMapper {
    int countByExample(MatchCommentLikeUserExample example);

    int deleteByExample(MatchCommentLikeUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MatchCommentLikeUser record);

    int insertSelective(MatchCommentLikeUser record);

    List<MatchCommentLikeUser> selectByExample(MatchCommentLikeUserExample example);

    MatchCommentLikeUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MatchCommentLikeUser record, @Param("example") MatchCommentLikeUserExample example);

    int updateByExample(@Param("record") MatchCommentLikeUser record, @Param("example") MatchCommentLikeUserExample example);

    int updateByPrimaryKeySelective(MatchCommentLikeUser record);

    int updateByPrimaryKey(MatchCommentLikeUser record);
}