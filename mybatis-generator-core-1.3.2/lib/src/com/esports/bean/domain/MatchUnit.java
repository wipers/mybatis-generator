package com.esports.bean.domain;

import java.util.Date;

public class MatchUnit {
    private Integer id;

    private Byte type;

    private Integer leftTeamId;

    private String leftTeamName;

    private String leftTeamBadge;

    private Integer leftTeamGoal;

    private Integer leftTeamAble;

    private Integer rightTeamId;

    private String rightTeamName;

    private String rightTeamBadge;

    private Integer rightTeamGoal;

    private Integer rightTeamAble;

    private Byte status;

    private Date beginTime;

    private Date createTime;

    private Date updateTime;

    private Date closeTime;

    private Integer groupId;

    private String groupName;

    private Integer esportType;

    private Integer recommend;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Integer getLeftTeamId() {
        return leftTeamId;
    }

    public void setLeftTeamId(Integer leftTeamId) {
        this.leftTeamId = leftTeamId;
    }

    public String getLeftTeamName() {
        return leftTeamName;
    }

    public void setLeftTeamName(String leftTeamName) {
        this.leftTeamName = leftTeamName == null ? null : leftTeamName.trim();
    }

    public String getLeftTeamBadge() {
        return leftTeamBadge;
    }

    public void setLeftTeamBadge(String leftTeamBadge) {
        this.leftTeamBadge = leftTeamBadge == null ? null : leftTeamBadge.trim();
    }

    public Integer getLeftTeamGoal() {
        return leftTeamGoal;
    }

    public void setLeftTeamGoal(Integer leftTeamGoal) {
        this.leftTeamGoal = leftTeamGoal;
    }

    public Integer getLeftTeamAble() {
        return leftTeamAble;
    }

    public void setLeftTeamAble(Integer leftTeamAble) {
        this.leftTeamAble = leftTeamAble;
    }

    public Integer getRightTeamId() {
        return rightTeamId;
    }

    public void setRightTeamId(Integer rightTeamId) {
        this.rightTeamId = rightTeamId;
    }

    public String getRightTeamName() {
        return rightTeamName;
    }

    public void setRightTeamName(String rightTeamName) {
        this.rightTeamName = rightTeamName == null ? null : rightTeamName.trim();
    }

    public String getRightTeamBadge() {
        return rightTeamBadge;
    }

    public void setRightTeamBadge(String rightTeamBadge) {
        this.rightTeamBadge = rightTeamBadge == null ? null : rightTeamBadge.trim();
    }

    public Integer getRightTeamGoal() {
        return rightTeamGoal;
    }

    public void setRightTeamGoal(Integer rightTeamGoal) {
        this.rightTeamGoal = rightTeamGoal;
    }

    public Integer getRightTeamAble() {
        return rightTeamAble;
    }

    public void setRightTeamAble(Integer rightTeamAble) {
        this.rightTeamAble = rightTeamAble;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public Integer getEsportType() {
        return esportType;
    }

    public void setEsportType(Integer esportType) {
        this.esportType = esportType;
    }

    public Integer getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer recommend) {
        this.recommend = recommend;
    }
}