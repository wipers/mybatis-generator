package com.esports.bean.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MatchUnitExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MatchUnitExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Byte value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Byte value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Byte value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Byte value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Byte value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Byte> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Byte> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Byte value1, Byte value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdIsNull() {
            addCriterion("left_team_id is null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdIsNotNull() {
            addCriterion("left_team_id is not null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdEqualTo(Integer value) {
            addCriterion("left_team_id =", value, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdNotEqualTo(Integer value) {
            addCriterion("left_team_id <>", value, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdGreaterThan(Integer value) {
            addCriterion("left_team_id >", value, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("left_team_id >=", value, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdLessThan(Integer value) {
            addCriterion("left_team_id <", value, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdLessThanOrEqualTo(Integer value) {
            addCriterion("left_team_id <=", value, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdIn(List<Integer> values) {
            addCriterion("left_team_id in", values, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdNotIn(List<Integer> values) {
            addCriterion("left_team_id not in", values, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdBetween(Integer value1, Integer value2) {
            addCriterion("left_team_id between", value1, value2, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamIdNotBetween(Integer value1, Integer value2) {
            addCriterion("left_team_id not between", value1, value2, "leftTeamId");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameIsNull() {
            addCriterion("left_team_name is null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameIsNotNull() {
            addCriterion("left_team_name is not null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameEqualTo(String value) {
            addCriterion("left_team_name =", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameNotEqualTo(String value) {
            addCriterion("left_team_name <>", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameGreaterThan(String value) {
            addCriterion("left_team_name >", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameGreaterThanOrEqualTo(String value) {
            addCriterion("left_team_name >=", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameLessThan(String value) {
            addCriterion("left_team_name <", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameLessThanOrEqualTo(String value) {
            addCriterion("left_team_name <=", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameLike(String value) {
            addCriterion("left_team_name like", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameNotLike(String value) {
            addCriterion("left_team_name not like", value, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameIn(List<String> values) {
            addCriterion("left_team_name in", values, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameNotIn(List<String> values) {
            addCriterion("left_team_name not in", values, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameBetween(String value1, String value2) {
            addCriterion("left_team_name between", value1, value2, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamNameNotBetween(String value1, String value2) {
            addCriterion("left_team_name not between", value1, value2, "leftTeamName");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeIsNull() {
            addCriterion("left_team_badge is null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeIsNotNull() {
            addCriterion("left_team_badge is not null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeEqualTo(String value) {
            addCriterion("left_team_badge =", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeNotEqualTo(String value) {
            addCriterion("left_team_badge <>", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeGreaterThan(String value) {
            addCriterion("left_team_badge >", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeGreaterThanOrEqualTo(String value) {
            addCriterion("left_team_badge >=", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeLessThan(String value) {
            addCriterion("left_team_badge <", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeLessThanOrEqualTo(String value) {
            addCriterion("left_team_badge <=", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeLike(String value) {
            addCriterion("left_team_badge like", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeNotLike(String value) {
            addCriterion("left_team_badge not like", value, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeIn(List<String> values) {
            addCriterion("left_team_badge in", values, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeNotIn(List<String> values) {
            addCriterion("left_team_badge not in", values, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeBetween(String value1, String value2) {
            addCriterion("left_team_badge between", value1, value2, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamBadgeNotBetween(String value1, String value2) {
            addCriterion("left_team_badge not between", value1, value2, "leftTeamBadge");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalIsNull() {
            addCriterion("left_team_goal is null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalIsNotNull() {
            addCriterion("left_team_goal is not null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalEqualTo(Integer value) {
            addCriterion("left_team_goal =", value, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalNotEqualTo(Integer value) {
            addCriterion("left_team_goal <>", value, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalGreaterThan(Integer value) {
            addCriterion("left_team_goal >", value, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalGreaterThanOrEqualTo(Integer value) {
            addCriterion("left_team_goal >=", value, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalLessThan(Integer value) {
            addCriterion("left_team_goal <", value, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalLessThanOrEqualTo(Integer value) {
            addCriterion("left_team_goal <=", value, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalIn(List<Integer> values) {
            addCriterion("left_team_goal in", values, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalNotIn(List<Integer> values) {
            addCriterion("left_team_goal not in", values, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalBetween(Integer value1, Integer value2) {
            addCriterion("left_team_goal between", value1, value2, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamGoalNotBetween(Integer value1, Integer value2) {
            addCriterion("left_team_goal not between", value1, value2, "leftTeamGoal");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleIsNull() {
            addCriterion("left_team_able is null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleIsNotNull() {
            addCriterion("left_team_able is not null");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleEqualTo(Integer value) {
            addCriterion("left_team_able =", value, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleNotEqualTo(Integer value) {
            addCriterion("left_team_able <>", value, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleGreaterThan(Integer value) {
            addCriterion("left_team_able >", value, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleGreaterThanOrEqualTo(Integer value) {
            addCriterion("left_team_able >=", value, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleLessThan(Integer value) {
            addCriterion("left_team_able <", value, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleLessThanOrEqualTo(Integer value) {
            addCriterion("left_team_able <=", value, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleIn(List<Integer> values) {
            addCriterion("left_team_able in", values, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleNotIn(List<Integer> values) {
            addCriterion("left_team_able not in", values, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleBetween(Integer value1, Integer value2) {
            addCriterion("left_team_able between", value1, value2, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andLeftTeamAbleNotBetween(Integer value1, Integer value2) {
            addCriterion("left_team_able not between", value1, value2, "leftTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdIsNull() {
            addCriterion("right_team_id is null");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdIsNotNull() {
            addCriterion("right_team_id is not null");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdEqualTo(Integer value) {
            addCriterion("right_team_id =", value, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdNotEqualTo(Integer value) {
            addCriterion("right_team_id <>", value, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdGreaterThan(Integer value) {
            addCriterion("right_team_id >", value, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("right_team_id >=", value, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdLessThan(Integer value) {
            addCriterion("right_team_id <", value, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdLessThanOrEqualTo(Integer value) {
            addCriterion("right_team_id <=", value, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdIn(List<Integer> values) {
            addCriterion("right_team_id in", values, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdNotIn(List<Integer> values) {
            addCriterion("right_team_id not in", values, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdBetween(Integer value1, Integer value2) {
            addCriterion("right_team_id between", value1, value2, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamIdNotBetween(Integer value1, Integer value2) {
            addCriterion("right_team_id not between", value1, value2, "rightTeamId");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameIsNull() {
            addCriterion("right_team_name is null");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameIsNotNull() {
            addCriterion("right_team_name is not null");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameEqualTo(String value) {
            addCriterion("right_team_name =", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameNotEqualTo(String value) {
            addCriterion("right_team_name <>", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameGreaterThan(String value) {
            addCriterion("right_team_name >", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameGreaterThanOrEqualTo(String value) {
            addCriterion("right_team_name >=", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameLessThan(String value) {
            addCriterion("right_team_name <", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameLessThanOrEqualTo(String value) {
            addCriterion("right_team_name <=", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameLike(String value) {
            addCriterion("right_team_name like", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameNotLike(String value) {
            addCriterion("right_team_name not like", value, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameIn(List<String> values) {
            addCriterion("right_team_name in", values, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameNotIn(List<String> values) {
            addCriterion("right_team_name not in", values, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameBetween(String value1, String value2) {
            addCriterion("right_team_name between", value1, value2, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamNameNotBetween(String value1, String value2) {
            addCriterion("right_team_name not between", value1, value2, "rightTeamName");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeIsNull() {
            addCriterion("right_team_badge is null");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeIsNotNull() {
            addCriterion("right_team_badge is not null");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeEqualTo(String value) {
            addCriterion("right_team_badge =", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeNotEqualTo(String value) {
            addCriterion("right_team_badge <>", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeGreaterThan(String value) {
            addCriterion("right_team_badge >", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeGreaterThanOrEqualTo(String value) {
            addCriterion("right_team_badge >=", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeLessThan(String value) {
            addCriterion("right_team_badge <", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeLessThanOrEqualTo(String value) {
            addCriterion("right_team_badge <=", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeLike(String value) {
            addCriterion("right_team_badge like", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeNotLike(String value) {
            addCriterion("right_team_badge not like", value, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeIn(List<String> values) {
            addCriterion("right_team_badge in", values, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeNotIn(List<String> values) {
            addCriterion("right_team_badge not in", values, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeBetween(String value1, String value2) {
            addCriterion("right_team_badge between", value1, value2, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamBadgeNotBetween(String value1, String value2) {
            addCriterion("right_team_badge not between", value1, value2, "rightTeamBadge");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalIsNull() {
            addCriterion("right_team_goal is null");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalIsNotNull() {
            addCriterion("right_team_goal is not null");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalEqualTo(Integer value) {
            addCriterion("right_team_goal =", value, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalNotEqualTo(Integer value) {
            addCriterion("right_team_goal <>", value, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalGreaterThan(Integer value) {
            addCriterion("right_team_goal >", value, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalGreaterThanOrEqualTo(Integer value) {
            addCriterion("right_team_goal >=", value, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalLessThan(Integer value) {
            addCriterion("right_team_goal <", value, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalLessThanOrEqualTo(Integer value) {
            addCriterion("right_team_goal <=", value, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalIn(List<Integer> values) {
            addCriterion("right_team_goal in", values, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalNotIn(List<Integer> values) {
            addCriterion("right_team_goal not in", values, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalBetween(Integer value1, Integer value2) {
            addCriterion("right_team_goal between", value1, value2, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamGoalNotBetween(Integer value1, Integer value2) {
            addCriterion("right_team_goal not between", value1, value2, "rightTeamGoal");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleIsNull() {
            addCriterion("right_team_able is null");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleIsNotNull() {
            addCriterion("right_team_able is not null");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleEqualTo(Integer value) {
            addCriterion("right_team_able =", value, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleNotEqualTo(Integer value) {
            addCriterion("right_team_able <>", value, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleGreaterThan(Integer value) {
            addCriterion("right_team_able >", value, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleGreaterThanOrEqualTo(Integer value) {
            addCriterion("right_team_able >=", value, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleLessThan(Integer value) {
            addCriterion("right_team_able <", value, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleLessThanOrEqualTo(Integer value) {
            addCriterion("right_team_able <=", value, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleIn(List<Integer> values) {
            addCriterion("right_team_able in", values, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleNotIn(List<Integer> values) {
            addCriterion("right_team_able not in", values, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleBetween(Integer value1, Integer value2) {
            addCriterion("right_team_able between", value1, value2, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andRightTeamAbleNotBetween(Integer value1, Integer value2) {
            addCriterion("right_team_able not between", value1, value2, "rightTeamAble");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andBeginTimeIsNull() {
            addCriterion("begin_time is null");
            return (Criteria) this;
        }

        public Criteria andBeginTimeIsNotNull() {
            addCriterion("begin_time is not null");
            return (Criteria) this;
        }

        public Criteria andBeginTimeEqualTo(Date value) {
            addCriterion("begin_time =", value, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeNotEqualTo(Date value) {
            addCriterion("begin_time <>", value, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeGreaterThan(Date value) {
            addCriterion("begin_time >", value, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("begin_time >=", value, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeLessThan(Date value) {
            addCriterion("begin_time <", value, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeLessThanOrEqualTo(Date value) {
            addCriterion("begin_time <=", value, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeIn(List<Date> values) {
            addCriterion("begin_time in", values, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeNotIn(List<Date> values) {
            addCriterion("begin_time not in", values, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeBetween(Date value1, Date value2) {
            addCriterion("begin_time between", value1, value2, "beginTime");
            return (Criteria) this;
        }

        public Criteria andBeginTimeNotBetween(Date value1, Date value2) {
            addCriterion("begin_time not between", value1, value2, "beginTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeIsNull() {
            addCriterion("close_time is null");
            return (Criteria) this;
        }

        public Criteria andCloseTimeIsNotNull() {
            addCriterion("close_time is not null");
            return (Criteria) this;
        }

        public Criteria andCloseTimeEqualTo(Date value) {
            addCriterion("close_time =", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeNotEqualTo(Date value) {
            addCriterion("close_time <>", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeGreaterThan(Date value) {
            addCriterion("close_time >", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("close_time >=", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeLessThan(Date value) {
            addCriterion("close_time <", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeLessThanOrEqualTo(Date value) {
            addCriterion("close_time <=", value, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeIn(List<Date> values) {
            addCriterion("close_time in", values, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeNotIn(List<Date> values) {
            addCriterion("close_time not in", values, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeBetween(Date value1, Date value2) {
            addCriterion("close_time between", value1, value2, "closeTime");
            return (Criteria) this;
        }

        public Criteria andCloseTimeNotBetween(Date value1, Date value2) {
            addCriterion("close_time not between", value1, value2, "closeTime");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNull() {
            addCriterion("group_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNotNull() {
            addCriterion("group_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupIdEqualTo(Integer value) {
            addCriterion("group_id =", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotEqualTo(Integer value) {
            addCriterion("group_id <>", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThan(Integer value) {
            addCriterion("group_id >", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("group_id >=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThan(Integer value) {
            addCriterion("group_id <", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThanOrEqualTo(Integer value) {
            addCriterion("group_id <=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdIn(List<Integer> values) {
            addCriterion("group_id in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotIn(List<Integer> values) {
            addCriterion("group_id not in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdBetween(Integer value1, Integer value2) {
            addCriterion("group_id between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotBetween(Integer value1, Integer value2) {
            addCriterion("group_id not between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupNameIsNull() {
            addCriterion("group_name is null");
            return (Criteria) this;
        }

        public Criteria andGroupNameIsNotNull() {
            addCriterion("group_name is not null");
            return (Criteria) this;
        }

        public Criteria andGroupNameEqualTo(String value) {
            addCriterion("group_name =", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotEqualTo(String value) {
            addCriterion("group_name <>", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameGreaterThan(String value) {
            addCriterion("group_name >", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameGreaterThanOrEqualTo(String value) {
            addCriterion("group_name >=", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLessThan(String value) {
            addCriterion("group_name <", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLessThanOrEqualTo(String value) {
            addCriterion("group_name <=", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameLike(String value) {
            addCriterion("group_name like", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotLike(String value) {
            addCriterion("group_name not like", value, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameIn(List<String> values) {
            addCriterion("group_name in", values, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotIn(List<String> values) {
            addCriterion("group_name not in", values, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameBetween(String value1, String value2) {
            addCriterion("group_name between", value1, value2, "groupName");
            return (Criteria) this;
        }

        public Criteria andGroupNameNotBetween(String value1, String value2) {
            addCriterion("group_name not between", value1, value2, "groupName");
            return (Criteria) this;
        }

        public Criteria andEsportTypeIsNull() {
            addCriterion("esport_type is null");
            return (Criteria) this;
        }

        public Criteria andEsportTypeIsNotNull() {
            addCriterion("esport_type is not null");
            return (Criteria) this;
        }

        public Criteria andEsportTypeEqualTo(Integer value) {
            addCriterion("esport_type =", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeNotEqualTo(Integer value) {
            addCriterion("esport_type <>", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeGreaterThan(Integer value) {
            addCriterion("esport_type >", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("esport_type >=", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeLessThan(Integer value) {
            addCriterion("esport_type <", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeLessThanOrEqualTo(Integer value) {
            addCriterion("esport_type <=", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeIn(List<Integer> values) {
            addCriterion("esport_type in", values, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeNotIn(List<Integer> values) {
            addCriterion("esport_type not in", values, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeBetween(Integer value1, Integer value2) {
            addCriterion("esport_type between", value1, value2, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("esport_type not between", value1, value2, "esportType");
            return (Criteria) this;
        }

        public Criteria andRecommendIsNull() {
            addCriterion("recommend is null");
            return (Criteria) this;
        }

        public Criteria andRecommendIsNotNull() {
            addCriterion("recommend is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendEqualTo(Integer value) {
            addCriterion("recommend =", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotEqualTo(Integer value) {
            addCriterion("recommend <>", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendGreaterThan(Integer value) {
            addCriterion("recommend >", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendGreaterThanOrEqualTo(Integer value) {
            addCriterion("recommend >=", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendLessThan(Integer value) {
            addCriterion("recommend <", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendLessThanOrEqualTo(Integer value) {
            addCriterion("recommend <=", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendIn(List<Integer> values) {
            addCriterion("recommend in", values, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotIn(List<Integer> values) {
            addCriterion("recommend not in", values, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendBetween(Integer value1, Integer value2) {
            addCriterion("recommend between", value1, value2, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotBetween(Integer value1, Integer value2) {
            addCriterion("recommend not between", value1, value2, "recommend");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}