package com.esports.bean.domain;

public class MatchRoundGames {
    private Integer id;

    private String name;

    private Integer status;

    private Integer matchUnitId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getMatchUnitId() {
        return matchUnitId;
    }

    public void setMatchUnitId(Integer matchUnitId) {
        this.matchUnitId = matchUnitId;
    }
}