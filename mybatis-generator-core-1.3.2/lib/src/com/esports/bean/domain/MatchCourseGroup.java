package com.esports.bean.domain;

import java.util.Date;

public class MatchCourseGroup {
    private Integer id;

    private Integer matchCourseId;

    private String name;

    private Integer sort;

    private Date createTime;

    private Date updateTime;

    private Date status;

    private Integer esportType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMatchCourseId() {
        return matchCourseId;
    }

    public void setMatchCourseId(Integer matchCourseId) {
        this.matchCourseId = matchCourseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getStatus() {
        return status;
    }

    public void setStatus(Date status) {
        this.status = status;
    }

    public Integer getEsportType() {
        return esportType;
    }

    public void setEsportType(Integer esportType) {
        this.esportType = esportType;
    }
}