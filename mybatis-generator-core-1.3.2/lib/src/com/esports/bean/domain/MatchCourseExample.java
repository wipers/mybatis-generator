package com.esports.bean.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MatchCourseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MatchCourseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMatchIdIsNull() {
            addCriterion("match_id is null");
            return (Criteria) this;
        }

        public Criteria andMatchIdIsNotNull() {
            addCriterion("match_id is not null");
            return (Criteria) this;
        }

        public Criteria andMatchIdEqualTo(Integer value) {
            addCriterion("match_id =", value, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdNotEqualTo(Integer value) {
            addCriterion("match_id <>", value, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdGreaterThan(Integer value) {
            addCriterion("match_id >", value, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("match_id >=", value, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdLessThan(Integer value) {
            addCriterion("match_id <", value, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdLessThanOrEqualTo(Integer value) {
            addCriterion("match_id <=", value, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdIn(List<Integer> values) {
            addCriterion("match_id in", values, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdNotIn(List<Integer> values) {
            addCriterion("match_id not in", values, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdBetween(Integer value1, Integer value2) {
            addCriterion("match_id between", value1, value2, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchIdNotBetween(Integer value1, Integer value2) {
            addCriterion("match_id not between", value1, value2, "matchId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdIsNull() {
            addCriterion("match_group_id is null");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdIsNotNull() {
            addCriterion("match_group_id is not null");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdEqualTo(Integer value) {
            addCriterion("match_group_id =", value, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdNotEqualTo(Integer value) {
            addCriterion("match_group_id <>", value, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdGreaterThan(Integer value) {
            addCriterion("match_group_id >", value, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("match_group_id >=", value, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdLessThan(Integer value) {
            addCriterion("match_group_id <", value, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdLessThanOrEqualTo(Integer value) {
            addCriterion("match_group_id <=", value, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdIn(List<Integer> values) {
            addCriterion("match_group_id in", values, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdNotIn(List<Integer> values) {
            addCriterion("match_group_id not in", values, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdBetween(Integer value1, Integer value2) {
            addCriterion("match_group_id between", value1, value2, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchGroupIdNotBetween(Integer value1, Integer value2) {
            addCriterion("match_group_id not between", value1, value2, "matchGroupId");
            return (Criteria) this;
        }

        public Criteria andMatchSortIsNull() {
            addCriterion("match_sort is null");
            return (Criteria) this;
        }

        public Criteria andMatchSortIsNotNull() {
            addCriterion("match_sort is not null");
            return (Criteria) this;
        }

        public Criteria andMatchSortEqualTo(Integer value) {
            addCriterion("match_sort =", value, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortNotEqualTo(Integer value) {
            addCriterion("match_sort <>", value, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortGreaterThan(Integer value) {
            addCriterion("match_sort >", value, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("match_sort >=", value, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortLessThan(Integer value) {
            addCriterion("match_sort <", value, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortLessThanOrEqualTo(Integer value) {
            addCriterion("match_sort <=", value, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortIn(List<Integer> values) {
            addCriterion("match_sort in", values, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortNotIn(List<Integer> values) {
            addCriterion("match_sort not in", values, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortBetween(Integer value1, Integer value2) {
            addCriterion("match_sort between", value1, value2, "matchSort");
            return (Criteria) this;
        }

        public Criteria andMatchSortNotBetween(Integer value1, Integer value2) {
            addCriterion("match_sort not between", value1, value2, "matchSort");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andEsportTypeIsNull() {
            addCriterion("esport_type is null");
            return (Criteria) this;
        }

        public Criteria andEsportTypeIsNotNull() {
            addCriterion("esport_type is not null");
            return (Criteria) this;
        }

        public Criteria andEsportTypeEqualTo(Integer value) {
            addCriterion("esport_type =", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeNotEqualTo(Integer value) {
            addCriterion("esport_type <>", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeGreaterThan(Integer value) {
            addCriterion("esport_type >", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("esport_type >=", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeLessThan(Integer value) {
            addCriterion("esport_type <", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeLessThanOrEqualTo(Integer value) {
            addCriterion("esport_type <=", value, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeIn(List<Integer> values) {
            addCriterion("esport_type in", values, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeNotIn(List<Integer> values) {
            addCriterion("esport_type not in", values, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeBetween(Integer value1, Integer value2) {
            addCriterion("esport_type between", value1, value2, "esportType");
            return (Criteria) this;
        }

        public Criteria andEsportTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("esport_type not between", value1, value2, "esportType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}