package com.esports.bean.domain;

public class MatchCommentLikeUser {
    private Integer id;

    private Integer matchCommentId;

    private Integer userId;

    private String userName;

    private String userAvatar;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMatchCommentId() {
        return matchCommentId;
    }

    public void setMatchCommentId(Integer matchCommentId) {
        this.matchCommentId = matchCommentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar == null ? null : userAvatar.trim();
    }
}