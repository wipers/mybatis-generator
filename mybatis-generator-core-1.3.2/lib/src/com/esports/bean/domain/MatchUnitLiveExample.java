package com.esports.bean.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MatchUnitLiveExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MatchUnitLiveExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdIsNull() {
            addCriterion("match_round_games_id is null");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdIsNotNull() {
            addCriterion("match_round_games_id is not null");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdEqualTo(Integer value) {
            addCriterion("match_round_games_id =", value, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdNotEqualTo(Integer value) {
            addCriterion("match_round_games_id <>", value, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdGreaterThan(Integer value) {
            addCriterion("match_round_games_id >", value, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("match_round_games_id >=", value, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdLessThan(Integer value) {
            addCriterion("match_round_games_id <", value, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdLessThanOrEqualTo(Integer value) {
            addCriterion("match_round_games_id <=", value, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdIn(List<Integer> values) {
            addCriterion("match_round_games_id in", values, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdNotIn(List<Integer> values) {
            addCriterion("match_round_games_id not in", values, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdBetween(Integer value1, Integer value2) {
            addCriterion("match_round_games_id between", value1, value2, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andMatchRoundGamesIdNotBetween(Integer value1, Integer value2) {
            addCriterion("match_round_games_id not between", value1, value2, "matchRoundGamesId");
            return (Criteria) this;
        }

        public Criteria andLiverNameIsNull() {
            addCriterion("liver_name is null");
            return (Criteria) this;
        }

        public Criteria andLiverNameIsNotNull() {
            addCriterion("liver_name is not null");
            return (Criteria) this;
        }

        public Criteria andLiverNameEqualTo(String value) {
            addCriterion("liver_name =", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameNotEqualTo(String value) {
            addCriterion("liver_name <>", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameGreaterThan(String value) {
            addCriterion("liver_name >", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameGreaterThanOrEqualTo(String value) {
            addCriterion("liver_name >=", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameLessThan(String value) {
            addCriterion("liver_name <", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameLessThanOrEqualTo(String value) {
            addCriterion("liver_name <=", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameLike(String value) {
            addCriterion("liver_name like", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameNotLike(String value) {
            addCriterion("liver_name not like", value, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameIn(List<String> values) {
            addCriterion("liver_name in", values, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameNotIn(List<String> values) {
            addCriterion("liver_name not in", values, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameBetween(String value1, String value2) {
            addCriterion("liver_name between", value1, value2, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverNameNotBetween(String value1, String value2) {
            addCriterion("liver_name not between", value1, value2, "liverName");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarIsNull() {
            addCriterion("liver_avatar is null");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarIsNotNull() {
            addCriterion("liver_avatar is not null");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarEqualTo(String value) {
            addCriterion("liver_avatar =", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarNotEqualTo(String value) {
            addCriterion("liver_avatar <>", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarGreaterThan(String value) {
            addCriterion("liver_avatar >", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarGreaterThanOrEqualTo(String value) {
            addCriterion("liver_avatar >=", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarLessThan(String value) {
            addCriterion("liver_avatar <", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarLessThanOrEqualTo(String value) {
            addCriterion("liver_avatar <=", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarLike(String value) {
            addCriterion("liver_avatar like", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarNotLike(String value) {
            addCriterion("liver_avatar not like", value, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarIn(List<String> values) {
            addCriterion("liver_avatar in", values, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarNotIn(List<String> values) {
            addCriterion("liver_avatar not in", values, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarBetween(String value1, String value2) {
            addCriterion("liver_avatar between", value1, value2, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andLiverAvatarNotBetween(String value1, String value2) {
            addCriterion("liver_avatar not between", value1, value2, "liverAvatar");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andImageIdIsNull() {
            addCriterion("image_id is null");
            return (Criteria) this;
        }

        public Criteria andImageIdIsNotNull() {
            addCriterion("image_id is not null");
            return (Criteria) this;
        }

        public Criteria andImageIdEqualTo(Integer value) {
            addCriterion("image_id =", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdNotEqualTo(Integer value) {
            addCriterion("image_id <>", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdGreaterThan(Integer value) {
            addCriterion("image_id >", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_id >=", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdLessThan(Integer value) {
            addCriterion("image_id <", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdLessThanOrEqualTo(Integer value) {
            addCriterion("image_id <=", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdIn(List<Integer> values) {
            addCriterion("image_id in", values, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdNotIn(List<Integer> values) {
            addCriterion("image_id not in", values, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdBetween(Integer value1, Integer value2) {
            addCriterion("image_id between", value1, value2, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("image_id not between", value1, value2, "imageId");
            return (Criteria) this;
        }

        public Criteria andGoalStrIsNull() {
            addCriterion("goal_str is null");
            return (Criteria) this;
        }

        public Criteria andGoalStrIsNotNull() {
            addCriterion("goal_str is not null");
            return (Criteria) this;
        }

        public Criteria andGoalStrEqualTo(String value) {
            addCriterion("goal_str =", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrNotEqualTo(String value) {
            addCriterion("goal_str <>", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrGreaterThan(String value) {
            addCriterion("goal_str >", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrGreaterThanOrEqualTo(String value) {
            addCriterion("goal_str >=", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrLessThan(String value) {
            addCriterion("goal_str <", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrLessThanOrEqualTo(String value) {
            addCriterion("goal_str <=", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrLike(String value) {
            addCriterion("goal_str like", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrNotLike(String value) {
            addCriterion("goal_str not like", value, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrIn(List<String> values) {
            addCriterion("goal_str in", values, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrNotIn(List<String> values) {
            addCriterion("goal_str not in", values, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrBetween(String value1, String value2) {
            addCriterion("goal_str between", value1, value2, "goalStr");
            return (Criteria) this;
        }

        public Criteria andGoalStrNotBetween(String value1, String value2) {
            addCriterion("goal_str not between", value1, value2, "goalStr");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeIsNull() {
            addCriterion("live_time is null");
            return (Criteria) this;
        }

        public Criteria andLiveTimeIsNotNull() {
            addCriterion("live_time is not null");
            return (Criteria) this;
        }

        public Criteria andLiveTimeEqualTo(Date value) {
            addCriterion("live_time =", value, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeNotEqualTo(Date value) {
            addCriterion("live_time <>", value, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeGreaterThan(Date value) {
            addCriterion("live_time >", value, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("live_time >=", value, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeLessThan(Date value) {
            addCriterion("live_time <", value, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeLessThanOrEqualTo(Date value) {
            addCriterion("live_time <=", value, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeIn(List<Date> values) {
            addCriterion("live_time in", values, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeNotIn(List<Date> values) {
            addCriterion("live_time not in", values, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeBetween(Date value1, Date value2) {
            addCriterion("live_time between", value1, value2, "liveTime");
            return (Criteria) this;
        }

        public Criteria andLiveTimeNotBetween(Date value1, Date value2) {
            addCriterion("live_time not between", value1, value2, "liveTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}