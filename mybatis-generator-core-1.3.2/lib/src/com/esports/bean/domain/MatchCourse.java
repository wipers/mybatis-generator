package com.esports.bean.domain;

import java.util.Date;

public class MatchCourse {
    private Integer id;

    private Integer matchId;

    private Integer matchGroupId;

    private Integer matchSort;

    private Date createTime;

    private Date updateTime;

    private Integer esportType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public Integer getMatchGroupId() {
        return matchGroupId;
    }

    public void setMatchGroupId(Integer matchGroupId) {
        this.matchGroupId = matchGroupId;
    }

    public Integer getMatchSort() {
        return matchSort;
    }

    public void setMatchSort(Integer matchSort) {
        this.matchSort = matchSort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getEsportType() {
        return esportType;
    }

    public void setEsportType(Integer esportType) {
        this.esportType = esportType;
    }
}