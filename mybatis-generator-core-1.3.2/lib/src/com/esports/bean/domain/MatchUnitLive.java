package com.esports.bean.domain;

import java.util.Date;

public class MatchUnitLive {
    private Integer id;

    private Integer matchRoundGamesId;

    private String liverName;

    private String liverAvatar;

    private String content;

    private Integer imageId;

    private String goalStr;

    private Date createTime;

    private Date updateTime;

    private Date liveTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMatchRoundGamesId() {
        return matchRoundGamesId;
    }

    public void setMatchRoundGamesId(Integer matchRoundGamesId) {
        this.matchRoundGamesId = matchRoundGamesId;
    }

    public String getLiverName() {
        return liverName;
    }

    public void setLiverName(String liverName) {
        this.liverName = liverName == null ? null : liverName.trim();
    }

    public String getLiverAvatar() {
        return liverAvatar;
    }

    public void setLiverAvatar(String liverAvatar) {
        this.liverAvatar = liverAvatar == null ? null : liverAvatar.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getGoalStr() {
        return goalStr;
    }

    public void setGoalStr(String goalStr) {
        this.goalStr = goalStr == null ? null : goalStr.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getLiveTime() {
        return liveTime;
    }

    public void setLiveTime(Date liveTime) {
        this.liveTime = liveTime;
    }
}